//namespace
var shoppingCart = shoppingCart || {};

shoppingCart.App = function(){
	var self = this;
	self.items = ko.observableArray([]);
	self.cart = new shoppingCart.Cart();

	self.buy = function(item){
		// add the item to cart
		self.cart.add(item);
		// update the number that purchased
		item.buy();
	};

	self.init = function(items){

		for(var i = 0,len=items.length;i<len;i++){
			var item = new shoppingCart.Item(items[i]);
			self.items().push(item);
		}
	};
};