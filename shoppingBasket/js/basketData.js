//namespace
var shoppingCart = shoppingCart || {};

shoppingCart.Cart = function(){
	var self = this;

	//List item that is already purchased
	self.items = ko.observableArray([]);

	//Cart is empty (true) or not (false)
	self.isEmpty = ko.computed(function(){
		return (self.items().length === 0);
	});

	//Add the item to the list of items which purchased
	self.add = function(item){
		// find the item that is the same id
        // The list of items that you've purchased
		var it = ko.utils.arrayFirst(self.items(),function(obj){
			return (obj.id === item.id);
		});

		// if the items are not in
        // list item that was purchased
		if(it === null){
			// add the item to the list
			self.items.push(item);

			// detection number that purchased
            // if the numbers 0,
            // delete from the list of items
			item.bought.subscribe(function(newValue){
				if(parseInt(newValue) === 0){
					self.items.remove(item);
				}
			});
		}
	};

	//Total
	self.total = ko.computed(function(){
		var total = 0;
		ko.utils.arrayForEach(self.items(),function(item){
			if(item.bought()){
				total += item.subtotal();
			}
		});
		return total;
	});
};
