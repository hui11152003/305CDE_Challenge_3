//namespace
var shoppingCart = shoppingCart || {};

shoppingCart.Item = function(data){
	var self = this;
	self.id = data.id;
	self.name = data.name;
	self.price = data.price;
    self.remark = data.remark;

	//Initial stock value
	var _originalStock = data.stock;

	//Stock's left
	self.stock = ko.observable(data.stock);

	//The number that is already purchased
	self.bought = ko.observable(0);

	//Detection of changes in the amount of which was purchased
    // For updates remaining stock
	self.bought.subscribe(function(newValue){

		// if newValue of type string
        // modified use text input, need
        // dijadiin first integer
		var val = parseInt(newValue);

		// if val is greater than the initial stock,
        // samain with initial stock, so
        // No. of max that can be purchased together
        // with the initial stock
		if(val > _originalStock){
			val = _originalStock;
			self.bought(val);
			return;
		}
		self.stock(_originalStock - val);
	});

	//number that purchased
	self.buy = function(){
		//self.bought() bertipe string kalo
		//diubah pake text input
		//jadi perlu dijadiin integer dulu
		self.bought(parseInt(self.bought())+1);
	};

	//Cancel the purchase of the item.
	self.reset = function(){
		self.bought(0);
	};

	//There are stock (true) or empty (false)
	self.inStock = ko.computed(function(){
		return (self.stock() > 0);
	});

	//subtotal of item
	self.subtotal = ko.computed(function(){
		return self.bought() * self.price;
	});
};
